import {
  createParamDecorator,
  ExecutionContext,
  HttpException,
  UnauthorizedException,
} from '@nestjs/common';
import * as admin from 'firebase-admin';

export const Authorizer = createParamDecorator(
  (data: unknown, ctx: ExecutionContext) => {
    try {
      const request = ctx.switchToHttp().getRequest();
      const token = request.headers.authorization;
      return admin
        .auth()
        .verifyIdToken(token)
        .catch((e) => {
          throw new UnauthorizedException(e);
        });
    } catch (e) {
      console.log(e);
      throw new HttpException(e, 500);
    }
  },
);
