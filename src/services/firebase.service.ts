import { Injectable } from '@nestjs/common';
import * as admin from 'firebase-admin';

@Injectable()
export class FirebaseService {
  public app: admin.app.App;
  constructor() {
    this.app = admin.initializeApp();
  }
}
