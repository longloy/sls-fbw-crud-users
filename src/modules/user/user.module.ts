import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ValidateObjectId } from 'src/pipes/validate-id.pipe';
import { FirebaseService } from 'src/services/firebase.service';
import { ServiceModule } from 'src/services/service.module';
import { UserController } from './user.controller';
import { UserSchema } from './user.schema';
import { UserService } from './user.service';

@Module({
    imports: [
      // ValidateObjectId
      // MongooseModule.forFeature([{name: 'Profile', schema: UserSchema}]),
    ],
    
    controllers: [UserController],
    providers: [UserService
    // ,FirebaseService
  ],
    // exports: [ValidateObjectId]
  })
  export class UserModule {}
  