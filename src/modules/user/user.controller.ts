import { BadRequestException, Body, Controller, Delete, Get, HttpStatus, NotFoundException, Param, Patch, Post, Query, Res } from "@nestjs/common";
import { ValidateObjectId } from "src/pipes/validate-id.pipe";
import { CreateUserDto } from "./dto/create-user.dto";
import { UserService } from "./user.service";

@Controller('users')
export class UserController {
  constructor(private readonly usersService: UserService) { }


  @Get('get')
  async getAllUser() {
    const users = await this.usersService.getUsers();
    return users;
  }

  @Post()
  async createUser(@Res() res, @Body() createUserDTO: CreateUserDto) {
    const createData = await this.usersService.createUser(createUserDTO);
    return res.status(HttpStatus.OK).json({
      message: 'User has been successfully added!',
      user: createData,
    });
  }

  @Get('get/:id')
  getUser(@Param('id') userId: string) {
    return this.usersService.getSomeUser(userId);
  }

  @Patch('get/:id')
  async updateProduct(
    @Res() res,
    @Param('id') userId: string,
    @Body() createUserDto: CreateUserDto,
  ) {
    const updateUser = await this.usersService.updateUser(userId, createUserDto);
    if (!updateUser) {
      throw new NotFoundException('Movie does not exist!');
    }
    return res.status(HttpStatus.OK).json({
      message: 'User Data has been successfully updated!',
      movie: updateUser,
    });
  }

  @Delete(':id')
  async removeUser(@Param('id') userId: string) {
    await this.usersService.deleteUser(userId);
    return "Success delete $";
  }

  // @Get('verify')
  // getVerify(@Query('token') token): any {
  //   if(!token) {
  //     throw new BadRequestException('Need token in query string parameter')
  //   }
  //   return this.usersService.verify(token);
  // }

}


