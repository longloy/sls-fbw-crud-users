import { IsDateString, IsOptional, IsString, MinLength } from "class-validator";

export class CreateUserDto {
    @IsString()
    id: string;
  
    @IsString()
    // @MinLength(5)
    firstname: string;
  
    @IsString()
    lastname: string;
  
  }
  