// import { Schema } from "@nestjs/mongoose";

export const statusUserSchema: any = {
  type: Object,
  user_type: {
    type: String,
    enum: ['สมาชิกทั่วไปไม่มีสังกัด', 'สมาชิกทั่วไปมีสังกัด', 'เจ้าหน้าที่'],
    default: 'สมาชิกทั่วไปไม่มีสังกัด'
  },
  name: {
    type: String,
    enum: ['อุบาสก', 'อุบาสิกา ', 'ผู้นำบุญ', 'อาสาสมัคร', 'อื่นๆ'],
    default: 'อื่นๆ'
  },
  detail: {
    type: String,
    enum: [],
    default: ''
  },
  require: true
};

export const addressSchema: any = {
  type: Object,
  current: {
    number: String,
    street: String,
    city: String,
    province: String,
    zip: String,
    country: String
  }
};

export const imageUserSchema: any = {
  type: Object,
  schema: {
    cover_img_url:String,
    profile_img_url:String
    // cover_img_key: String,
    // cover_img_url: String,
    // profile_img_key: String,
    // profile_img_url: String
  }
};

export const belongSchema: any = {
  type: Object,
  main: {
    type: String,
    enum: ['สำนัก', 'ประเทศ', ''],
    default: ''
  },
  major: {
    type: String,
    enum: ['กอง', 'จังหวัด  ', 'เมือง ', 'นครหลวง', 'อื่นๆ'],
    default: 'อื่นๆ'
  },
  minor: {
    type: String,
    enum: ['แผนก', 'ตำบล', 'สาย'],
    default: '',
    description: String
  },
}

export const bioSchema: any = {
  type: Object,
  schema: {
    description: String,
    height: Number,
    weight: Number,
    good_habbit: String,
    bad_habbit: String,
    world_exp: String,
    temple_exp: String,
    disease: String,
  }
}