import * as mongoose from 'mongoose';
import { addressSchema, belongSchema, imageUserSchema, statusUserSchema } from './share.model';

export const UserSchema = new mongoose.Schema({
  id: {
    type: String,
    // required: true
  },
  auth_id: {
    type: String,
    // required: true,
  },
  // nickname:{type:String},
  firstname: {
    type: String,
    // required: true,
  },
  lastname: {
    type: String,
    // required: true,
  },
  nickname: String,
  birthday: Date,
  ordaindate: Date,
  age: String,
  email: String,
  lineId: String,
  images: imageUserSchema,     // imageSchema
  address: addressSchema,   // addressSchema
  telephone_number: String,
  status: statusUserSchema,
  belong: belongSchema,
  // policy: {
  //     ispublic: Boolean,
  //     default: true,
  // },
  created_at: Number,
  update_at: Number
});

export interface userInterface extends mongoose.Document {
  id: string;
  firstname: string;
  lastname: string;
  nickname: string;
  birthday: Date;
  ordaindate: Date;
  age: string;
  email: string;
  lineId: string;
  images: object;
  address: object;
  telephone_number: string;
  status: object;
  belong: object;

  // price: number;
}
