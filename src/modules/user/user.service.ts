import { Get, HttpStatus, Injectable, NotFoundException, Param, Res, UnauthorizedException } from '@nestjs/common';
import { Model } from "mongoose";
import { userInterface } from "./user.schema";
import { InjectModel } from "@nestjs/mongoose";
import { CreateUserDto } from './dto/create-user.dto';

@Injectable()
export class UserService {
  constructor(
    // public firebaseService: FirebaseService,
    @InjectModel('Profile') private userModel: Model<userInterface>,
  ) { }


  async getUsers() {
    const users = await this.userModel.find().exec();
    return users.map((res) => ({
      id: res._id,
      firstname: res.firstname,
      lastname: res.lastname,
      images: res.images,
      // ...prod
    }));
  }

  async getSomeUser(userID) {
    const fetchUser = await this.findUser(userID);
    return {
      ...fetchUser,
    };
  }

  async createUser(createUserDTO: CreateUserDto): Promise<userInterface> {
    const addUser = await new this.userModel(createUserDTO);
    return addUser.save();
  }

  async updateUser(userID, createUserDTO: CreateUserDto): Promise<userInterface> {
    const updateUser = await this.userModel.findByIdAndUpdate(
      userID,
      createUserDTO,
      { new: true },
    );
    return updateUser;
  }

  async deleteUser(userId): Promise<userInterface> {
    const deleteUser = await this.userModel.findByIdAndRemove(userId);
    return deleteUser;
  }


  // verify(token: string) {
  //   return this.firebaseService.app.auth()
  //     .verifyIdToken(token)
  //     .catch(e => {
  //       throw new UnauthorizedException(e);
  //     });
  // }


  private async findUser(_id: string): Promise<userInterface> {
    var user;
    try {
      user = await this.userModel.findById(_id).exec();
    } catch (error) {
      throw new NotFoundException('Could not find user.');
    }
    if (!user) {
      throw new NotFoundException('Could not find User.');
    }
    return user;
  }

}

