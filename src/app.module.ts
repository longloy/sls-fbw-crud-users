import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UserSchema } from "./modules/user/user.schema";
import { UserController } from "./modules/user/user.controller";
import { UserService } from "./modules/user/user.service";
import { UserModule } from './modules/user/user.module';
// import { ValidateObjectId } from './pipes/validate-id.pipe';


@Module({
  imports: [
    // UserModule,ValidateObjectId,
    MongooseModule.forFeature([{ name: 'Profile', schema: UserSchema }]),
    MongooseModule.forRoot(process.env.MONGODB_URL)],
  controllers: [UserController],
  providers: [UserService],
})
export class AppModule {}
